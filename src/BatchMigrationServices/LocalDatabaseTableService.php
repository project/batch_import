<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\Core\Database\Connection;

/**
 * LocalDatabaseTableService class.
 *
 * Updates batch_import table as entities are imported.
 */
class LocalDatabaseTableService extends MigrationServiceBase {

  /**
   * Connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'db_table';
  }

  /**
   * {@inheritdoc}
   */
  public static function serviceArguments() {
    return ['database'];
  }

  /**
   * Get the imported entity based on the source entity id.
   *
   * @param string $source_id
   *   ID of the entity in source.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle for entity.
   * @param string $origin
   *   Source id.
   *
   * @return array|null
   *   Array of destination ids.
   */
  public function load($source_id, $entity_type, $bundle, $origin = NULL) {
    return $this->database->query("
      SELECT eid_new
        FROM batch_import
       WHERE eid = :eid
         AND type = :type
         AND origin = :origin
         AND bundle = :bundle
    ", [
      ':eid' => $source_id,
      ':type' => $entity_type,
      ':origin' => $origin,
      ':bundle' => $bundle,
    ])->fetchCol();
  }

  /**
   * Get the imported entity based on the source entity id.
   *
   * @param string|array $dest_ids
   *   ID of the entity in destination.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle for entity.
   * @param string $origin
   *   Source id.
   *
   * @return array|null
   *   Array of source ids.
   */
  public function loadSource($dest_ids, $entity_type, $bundle, $origin = NULL) {
    if (is_string($dest_ids)) {
      $dest_ids = [$dest_ids];
    }

    return $this->database->query("
      SELECT eid
        FROM batch_import
       WHERE type = :type
         AND bundle = :bundle
         AND origin = :origin
         AND eid_new in (:eids[])
    ", [
      ':eids[]' => $dest_ids,
      ':type' => $entity_type,
      ':origin' => $origin,
      ':bundle' => $bundle,
    ])->fetchCol();
  }

  /**
   * Save imported entity in local database table.
   *
   * @param string $source_id
   *   ID of the entity in the source.
   * @param string $dest_id
   *   ID of the entity in the destination.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Entity bundle.
   * @param string $origin
   *   Source id.
   */
  public function save($source_id, $dest_id, $entity_type, $bundle, $origin = NULL) {
    $this->database->insert('batch_import')
      ->fields([
        'eid' => $source_id,
        'type' => $entity_type,
        'bundle' => $bundle,
        'eid_new' => $dest_id,
        'origin' => $origin,
      ])
      ->execute();
  }

  /**
   * Remove the imported entity in local database table.
   *
   * @param string $dest_id
   *   ID of the entity in the destination.
   * @param string $entity_type
   *   Entity type.
   */
  public function delete($dest_id, $entity_type) {
    $this->database->query("
      DELETE FROM batch_import
        WHERE eid_new = :dest_id
          AND type = :type
    ", [
      ':dest_id' => $dest_id,
      ':type' => $entity_type,
    ]);
  }

  /**
   * Set entity as entity referenced in local database table.
   *
   * @param string $source_id
   *   ID of the entity in the source.
   * @param string $entity_type
   *   Entity type.
   * @param string $origin
   *   Source id.
   */
  public function setEntityAsReferenced($source_id, $entity_type, $origin = NULL) {
    $this->database->query("
      UPDATE batch_import
         SET is_referenced = 1
       WHERE eid = :source_id
         AND type = :type
         AND origin = :origin
    ", [
      ':source_id' => $source_id,
      ':type' => $entity_type,
      ':origin' => $origin,
    ]);
  }

}
