<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Database\Connection;

/**
 * TermMigrationService class.
 *
 * Common functionality for taxonomy term imports.
 */
class TermMigrationService extends EntityMigrationServiceBase {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'taxonomy_term';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'tid';
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return $this->entityManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, array $args = []) {
    // Initial value for new taxonomy term.
    $values = [
      'name' => $data['name'],
      'vid' => $data['vocabulary'],
      'description' => !empty($data['description']) ? [
        'value' => $data['description'],
        'format' => 'full_html',
      ] : NULL,
    ];

    // Create and return new taxonomy term.
    return $this->storage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, array $args = []) {
    return $this->storage()->load($dest_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($term, $data, array $args = []) {
    return $term->save();
  }

  /**
   * Query and return a term tid with a matching name.
   *
   * @param string|null $vocab_name
   *   Machine name for taxonomy vocabulary.
   * @param string|null $term_name
   *   Human readable name of taxonomy term.
   *
   * @return string
   *   Return taxonomy term tid that matches the name.
   */
  public function getTidByName($vocab_name = NULL, $term_name = NULL) {
    $properties = [];

    // Properties to load term.
    if (!empty($term_name)) {
      $properties['name'] = $term_name;
    }
    if (!empty($vocab_name)) {
      $properties['vid'] = $vocab_name;
    }
    // Load taxonomy term.
    $terms = $this->storage()->loadByProperties($properties);
    $term = reset($terms);

    // Return tid.
    return !empty($term) ? $term->id() : 0;
  }

  /**
   * Query and return a term tid with a matching name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity that owns the field.
   * @param string $field_name
   *   Name of term reference field to set.
   * @param string $vocab_name
   *   Machine name for taxonomy vocabulary.
   * @param string $term_name
   *   Human readable name of taxonomy term.
   */
  public function setTermFieldByName(EntityInterface $entity, $field_name, $vocab_name, $term_name) {
    // Look up tid by name.
    $tid = $this->getTidByName($vocab_name, $term_name);
    // Load term.
    $term = Term::load($tid);

    // Set entity field with term.
    $entity->set($field_name, $term);
  }

  /**
   * Set multiple value taxonomy term field using data connection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection object.
   * @param array $tids
   *   Array of tids to use for importint data.
   * @param string $vocab
   *   Vocab machine name.
   * @param string $origin
   *   Origin key.
   *
   * @return string
   *   Alias string.
   */
  public function loadMultipleTermField(Connection $connection, array $tids, $vocab, $origin = NULL) {
    if (empty($tids)) {
      return [];
    }
    // Get term data from database with tids.
    $term_data = $this->getTermData($connection, $tids);

    $terms = [];
    // Loop through terms.
    foreach ($term_data as $data) {
      if (!isset($data['name'])) {
        continue;
      }
      // Load or create term entities.
      $terms[] = $this->getEntity($data, $vocab, NULL, $origin);
    }

    return $terms;
  }

  /**
   * Set multiple value taxonomy term field using data connection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection object.
   * @param array|string $tids
   *   Array of tids to use for importint data.
   *
   * @return array
   *   Array of taxonomy term data.
   */
  public function getTermData(Connection $connection, $tids) {
    if (empty($tids)) {
      return [];
    }
    if (is_string($tids)) {
      $tids = [$tids];
    }
    $term_data = [];

    // Loop through tids.
    foreach ($tids as $tid) {
      if (!is_numeric($tid)) {
        continue;
      }

      // Retrieve term data from database.
      $data = $connection
        ->query("SELECT name, description FROM taxonomy_term_data WHERE tid = :tid", [':tid' => $tid])
        ->fetchAll();

      $term_data[] = isset($data[0]) ? (array) $data[0] : NULL;
    }

    return $term_data;
  }

}
