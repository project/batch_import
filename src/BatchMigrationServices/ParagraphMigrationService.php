<?php

namespace Drupal\batch_import\BatchMigrationServices;

/**
 * ParagraphMigrationService class.
 *
 * Common functionality for paragraph imports.
 */
class ParagraphMigrationService extends EntityMigrationServiceBase {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'paragraph';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'pid';
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return $this->entityManager->getStorage('paragraph');
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, $args = []) {
    // Initialize values for new paragraph.
    $values = [
      'type' => $args['bundle'],
    ];

    // Create paragraph and return.
    return $this->storage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, $args = []) {
    return $this->storage()->load($dest_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($paragraph, $data, $args = []) {
    return $paragraph->save();
  }

}
