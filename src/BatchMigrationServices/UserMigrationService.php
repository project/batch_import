<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\User\Entity\User;

/**
 * UserMigrationService class.
 *
 * Common functionality for user imports.
 */
class UserMigrationService extends EntityMigrationServiceBase {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'uid';
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return $this->entityManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, $args = []) {
    // Initial values for new user.
    $values = [
      'langcode' => $data['langcode'] ?? 'en',
      'name' => $data['name'] ?? '',
      'pass' => $data['pass'] ?? '',
      'mail' => $data['mail'] ?? '',
      'timezone' => $data['timezone'] ?? '',
      'status' => $data['status'] ?? 1,
      'created' => $data['created'] ?? '',
      'changed' => $data['changed'] ?? $data['created'],
      'access' => $data['access'] ?? '',
      'login' => $data['login'] ?? '',
      'init' => $data['init'] ?? '',
    ];

    // Create and return new user.
    return $this->storage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, $args = []) {
    return $this->storage()->load($dest_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($user, $data, $args = []) {
    // Update password for user while saving.
    if ($result = $user->save()) {
      $this->updateUserPassword($data['pass'], $user->id());
    }

    return $result;
  }

  /**
   * Helper function to update user password to match old one.
   *
   * @param string $hash
   *   Password hash.
   * @param string $uid
   *   User id.
   *
   * @return int
   *   Number of rows that were updated.
   */
  public function updateUserPassword(string $hash, string $uid) {
    return \Drupal::database()->update('users_field_data')
      ->fields(['pass' => $hash])
      ->condition('uid', $uid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function checkForExisting(array $data, $bundle = NULL, $entity_type_id = NULL, $origin = NULL) {
    // Get list of destinaton ids from local db table.
    $dest_ids = parent::checkForExisting($data, $bundle, $entity_type_id, $origin);
    if (!empty($data['name'])) {
      // Query users by name.
      $results = \Drupal::entityQuery('user')
        ->accessCheck(FALSE)
        ->condition('name', $data['name'])
        ->execute();

      // Merge destination results.
      return array_merge($results, $dest_ids);
    }

    return $dest_ids;
  }

  /**
   * Load or create a dummy user.
   *
   * @param string $email
   *   Email address to use for dummy user.
   */
  public function getDummyUser($email) {
    // Load user using email address.
    $user = user_load_by_mail($email);
    if (!$user instanceof User) {
      // If user doesn't exists, create it.
      $user = User::create([
        'langcode' => 'en',
        'name' => $email,
        'pass' => \Drupal::getContainer()->get('uuid')->generate(),
        'mail' => $email,
        'status' => 1,
        'init' => $email,
      ]);
      $user->save();

    }
    return $user;
  }

}
