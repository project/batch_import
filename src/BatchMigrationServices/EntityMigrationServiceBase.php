<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Base class for Batch Import entity migration services.
 */
abstract class EntityMigrationServiceBase extends MigrationServiceBase implements EntityMigrationServiceInterface {

  /**
   * Retrieves entity type manager interface.
   *
   * @var \Drupal\batch_import\BatchMigrationServices\LocalDatabaseTableService
   */
  protected $dbTable;

  /**
   * Retrieves entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructor.
   *
   * @param \Drupal\batch_import\BatchMigrationServices\LocalDatabaseTableService $db_table
   *   Migration service for local database table.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity type manager.
   */
  public function __construct(LocalDatabaseTableService $db_table, EntityTypeManagerInterface $entity_manager) {
    $this->dbTable = $db_table;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function serviceArguments() {
    return [
      'batch_import.migration.db_table',
      'entity_type.manager',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkForExisting(array $data, $bundle = NULL, $entity_id_key = NULL, $origin = NULL) {
    $entity_id = $entity_id_key ?? $this->entityTypeId();
    // Source id from $data.
    $source_id = $data[$entity_id] ?? NULL;

    // Return empty if there's no source id.
    if (empty($source_id)) {
      return NULL;
    }

    // Load destination id array from db_table.
    return $this->dbTable->load($source_id, $this->id(), $bundle, $origin);
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntity(array &$data, $bundle = NULL, $entity_id_key = NULL, $origin = NULL) {
    $entity = NULL;
    // List of destination ids if they exist.
    $dest_ids = $this->checkForExisting($data, $bundle, $entity_id_key, $origin);

    if (!empty($dest_ids)) {
      foreach ($dest_ids as $id) {
        // Skip if $entity is already found.
        if (!empty($entity)) {
          continue;
        }

        // Set is_new for migration plugins.
        $data['is_new'] = FALSE;
        // Load entity instance.
        $entity = $this->load($id);
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(array &$data, $bundle = NULL, $entity_id_key = NULL, $origin = NULL) {
    $entity_id = $entity_id_key ?? $this->entityTypeId();

    // Make sure there's a source id.
    if (isset($entity_id) && !empty($data[$entity_id])) {
      // Load entity instance if it exists.
      $entity = $this->loadEntity($data, $bundle, $entity_id_key, $origin);

      // Otherwise create a new instance.
      if (empty($entity)) {
        // Set is_new for migration plugins.
        $data['is_new'] = TRUE;

        // Create new entity instance.
        $entity = $this->new($data, [
          'bundle' => $bundle,
        ]);
      }
    }

    return $entity ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function initEntity(ContentEntityBase $entity, array &$data, $origin = NULL) {
    return $entity;
  }

}
