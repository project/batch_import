<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines an interface for Batch Import entity migration services.
 */
interface EntityMigrationServiceInterface extends MigrationServiceInterface {

  /**
   * ID key for entity type. Ex. nid, tid, uid, etc.
   *
   * @return string
   *   Return entity type id machine name.
   */
  public function entityTypeId();

  /**
   * Return storage object for this particular entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityStorageBase
   *   Storage object for this entity.
   */
  public function storage();

  /**
   * Create a new instance of entity.
   *
   * @param array $data
   *   Data passed in from source.
   * @param array $args
   *   Any additional arguments needed to create entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   New entity.
   */
  public function new(array $data, array $args = []);

  /**
   * Load an existing instance of entity.
   *
   * @param string $dest_id
   *   ID of existing entity in destination.
   * @param array $args
   *   Any additional arguments needed to create entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Loaded entity.
   */
  public function load($dest_id, array $args = []);

  /**
   * Save entity instance.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Instance of entity.
   * @param array $data
   *   Data passed in from source.
   * @param array $args
   *   Any additional arguments needed to create entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Saved entity.
   */
  public function save(EntityInterface $entity, array $data, array $args = []);

  /**
   * Check if entity already exists in destination.
   *
   * @param array $data
   *   Data passed in from source.
   * @param string $bundle
   *   Bundle for entity, or NULL if not applicable.
   * @param string $entity_id_key
   *   Id override for entity type. Ex nid, tid, uid, etc.
   * @param string $origin
   *   Source origin id.
   *
   * @return array
   *   Return array of relevent destination ids.
   */
  public function checkForExisting(array $data, $bundle = NULL, $entity_id_key = NULL, $origin = NULL);

  /**
   * Load entity instance if it exists, otherwise create a new one.
   *
   * @param array $data
   *   Data passed in from source.
   * @param string $bundle
   *   Bundle for entity, or NULL if not applicable.
   * @param string $entity_id_key
   *   Override entity id key. Ex. nid, tid, uid, etc.
   * @param string $origin
   *   Source origin id.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Saved entity.
   */
  public function getEntity(array &$data, $bundle = NULL, $entity_id_key = NULL, $origin = NULL);

  /**
   * Initialize entity for import.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   Entity recently created or loaded.
   * @param array $data
   *   The info array.
   * @param string $origin
   *   Origin key.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The initialized entity.
   */
  public function initEntity(ContentEntityBase $entity, array &$data, $origin = NULL);

}
