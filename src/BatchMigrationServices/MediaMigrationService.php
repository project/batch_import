<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\media\Entity\Media;
use Drupal\Core\Entity\EntityInterface;

/**
 * MediaMigrationService class.
 *
 * Common functionality for media imports.
 */
class MediaMigrationService extends EntityMigrationServiceBase {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'mid';
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return $this->entityManager->getStorage('media');
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, $args = []) {
    // Initial values for new media.
    $values = [
      'bundle' => $args['bundle'],
      'langcode' => 'en',
    ];

    // If uuid is available.
    if (!empty($data['uuid'])) {
      $values['uuid'] = $data['uuid'];
    }

    // Create new media entity and return.
    return $this->storage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, $args = []) {
    return $this->storage()->load($dest_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($media, $data, $args = []) {
    $media->save();
  }

  /**
   * Set a media field using file id.
   *
   * @param array $data
   *   Source data array.
   * @param string $fid_key
   *   Key in $data for source file id.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity that owns the media field.
   * @param string $field_name
   *   Media field machine name.
   * @param string $bundle
   *   Bundle for media entity.
   * @param string $origin
   *   Origin id for source media.
   */
  public function setMediaField(array $data, $fid_key, EntityInterface $entity, $field_name, $bundle, $origin = NULL) {
    // Load entity if it exists.
    $image = $data[$fid_key] ? $this->loadEntity($data, $bundle, $fid_key, $origin) : NULL;
    if ($image instanceof Media) {
      // Set media entity as referenced in local db table.
      $this->dbTable->setEntityAsReferenced($data[$fid_key], $this->id(), $origin);
    }
    // Set entity field.
    $entity->set($field_name, $image ?? NULL);
  }

}
