<?php

namespace Drupal\batch_import\BatchMigrationServices;

/**
 * Defines an interface for Batch Import migration services.
 */
abstract class MigrationServiceBase implements MigrationServiceInterface {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function serviceArguments() {
    return [];
  }

}
