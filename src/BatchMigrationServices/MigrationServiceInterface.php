<?php

namespace Drupal\batch_import\BatchMigrationServices;

/**
 * Defines an interface for Batch Import migration plugins.
 */
interface MigrationServiceInterface {

  /**
   * Load entity instance if it exists, otherwise create a new one.
   *
   * @return string
   *   Return unique id for this migration service.
   */
  public static function id();

  /**
   * Return array of service ids passed into class constructor.
   *
   * @return array
   *   Service ids to pass to migration service class constructor.
   */
  public static function serviceArguments();

}
