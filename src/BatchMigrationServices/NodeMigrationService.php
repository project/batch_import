<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * NodeMigrationService class.
 *
 * Common functionality for node imports.
 */
class NodeMigrationService extends EntityMigrationServiceBase {

  /**
   * Retrieves entity type manager interface.
   *
   * @var \Drupal\batch_import\Services\UserMigrationService
   */
  protected $userMigrationService;

  /**
   * Constructor.
   *
   * @param \Drupal\batch_import\BatchMigrationServices\LocalDatabaseTableService $db_table
   *   Migration service for local database table.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity type manager.
   * @param \Drupal\batch_import\Services\UserMigrationService $user_migration_service
   *   User migration service.
   */
  public function __construct(LocalDatabaseTableService $db_table, EntityTypeManagerInterface $entity_manager, UserMigrationService $user_migration_service) {
    parent::__construct($db_table, $entity_manager);
    $this->userMigrationService = $user_migration_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'nid';
  }

  /**
   * {@inheritdoc}
   */
  public static function serviceArguments() {
    return [
      'batch_import.migration.db_table',
      'entity_type.manager',
      'batch_import.migration.user',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storage() {
    return $this->entityManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, $args = []) {
    if (empty($args['bundle'])) {
      return NULL;
    }

    // Load author user.
    $author = !empty($data['uid']) ? $this->userMigrationService->load($data['uid']) : NULL;

    // Initial values for a new node.
    $values = [
      'type' => $args['bundle'],
      'langcode' => 'en',
      'status' => $data['status'],
      'uid' => $author ? $author->id() : 1,
      'created' => $data['created'],
    ];

    // Create node and return.
    return $this->storage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, $args = []) {
    return $this->storage()->load($dest_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($node, $data, $args = []) {
    return $node->save();
  }

  /**
   * {@inheritdoc}
   */
  public function initEntity(ContentEntityBase $entity, array &$data, $origin = NULL) {
    $author = $this->userMigrationService->loadEntity($data, $origin);
    if (!empty($author)) {
      $entity->uid = $author->id();
    }

    // Node created property.
    $entity->set('created', $data['created']);

    // Node status property.
    if ($data['status'] == 1) {
      $entity->set('moderation_state', 'published');
    }

    // Node title property.
    $entity->setTitle($data['title']);

    return $entity;
  }

}
