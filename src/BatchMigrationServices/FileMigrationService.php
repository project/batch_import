<?php

namespace Drupal\batch_import\BatchMigrationServices;

use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * FileMigrationService class.
 *
 * Handle migration for managed files.
 * The files should already be in local files directory before migration.
 */
class FileMigrationService extends EntityMigrationServiceBase {

  /**
   * {@inheritdoc}
   */
  public static function id() {
    return 'file';
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'fid';
  }

  /**
   * {@inheritdoc}
   */
  public function new($data, $args = []) {
    // Initial values for new manage file.
    $values = [
      'uri' => $data['uri'],
      'uid' => $data['uid'],
      'status' => FileInterface::STATUS_PERMANENT,
    ];

    // Create new file and return.
    return File::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($dest_id, $args = []) {
    return !empty($dest_id) && is_numeric($dest_id) ?
      File::load($dest_id) :
      NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function save($file, $data, $args = []) {
    $file->uri = $data['uri'];
    $file->save();
  }

}
