<?php

namespace Drupal\batch_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\batch_import\MigrationActivationService;
use Drupal\batch_import\BatchMigrationsPluginManager;

/**
 * Controller for batch import form.
 */
class BatchImportForm extends ConfigFormBase {

  /**
   * Migration activation service.
   *
   * @var \Drupal\batch_import\MigrationActivationService
   */
  protected $migrationActivation;

  /**
   * Migration plugin manager.
   *
   * @var \Drupal\batch_import\BatchMigrationsPluginManager
   */
  protected $pluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\batch_import\MigrationActivationService $migration_activation
   *   Migration activation service.
   * @param \Drupal\batch_import\BatchMigrationsPluginManager $plugin_manager
   *   Migration plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MigrationActivationService $migration_activation, BatchMigrationsPluginManager $plugin_manager) {
    parent::__construct($config_factory);
    $this->migrationActivation = $migration_activation;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('migration_activation'),
      $container->get('plugin.manager.batch_migrations')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['batch_import.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'batch_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get lits of migration plugin options.
    $plugin_options = $this->getPluginOptions();

    // Table header labels.
    $header = [
      'name' => $this->t('Name'),
      'dependencies' => $this->t('Dependencies'),
    ];

    // Add table form for migration plugins.
    $form['migrations'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Migrations'),
      '#header' => $header,
      '#required' => TRUE,
      '#options' => $plugin_options,
    ];

    // Attach library.
    $form['#attached']['library'][] = 'batch_import/import_form';

    $form['actions']['#type'] = 'actions';
    $form['actions']['queue_import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Queue import for cron'),
      '#button_type' => 'primary',
      '#submit' => [[$this, 'queueSubmitForm']],
    ];
    $form['actions']['run_import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Manually run import'),
      '#button_type' => 'primary',
      '#submit' => [[$this, 'manualSubmitForm']],
    ];

    $form = parent::buildForm($form, $form_state);

    // Remove default submit.
    unset($form['actions']['submit']);

    return $form;
  }

  /**
   * Get list of migration plugin options, filtered, sorted, and formated.
   *
   * @return array
   *   Array of plugin migration options.
   */
  protected function getPluginOptions() {
    // Load all migration plugin definitions.
    $plugins = $this->pluginManager->getDefinitions();

    // Filter out hidden migrations.
    $plugins = array_filter($plugins, function ($plugin) {
      return !array_key_exists('hidden', $plugin) || !$plugin['hidden'];
    });

    // Sort plugin by dependencies.
    uasort($plugins, function ($a, $b) {
      // Make sure id properties are available.
      if (!empty($a['id']) && !empty($b['id'])) {
        // If both plugins have dependencies.
        if (!empty($a['dependencies']) && !empty($b['dependencies'])) {
          // If they are dependent on each other.
          // @todo Should show some feedback on the form in this case.
          if (array_key_exists($a['id'], $b['dependencies']) && array_key_exists($b['id'], $a['dependencies'])) {
            return 0;
          }
          // If b is dependent on a.
          elseif (array_key_exists($a['id'], $b['dependencies'])) {
            return -1;
          }
          // If a is dependent on b.
          elseif (array_key_exists($b['id'], $a['dependencies'])) {
            return 1;
          }
          // Neither are dependent on the other, sort by dependency count.
          else {
            if (count($a['dependencies']) < count($b['dependencies'])) {
              return -1;
            }
            elseif (count($a['dependencies']) > count($b['dependencies'])) {
              return 1;
            }
          }
        }
        // If a has dependencies and b doesn't, sort b first.
        elseif (!empty($a['dependencies'])) {
          return 1;
        }
        // If b has dependencies and a doesn't, sort a first.
        elseif (!empty($b['dependencies'])) {
          return -1;
        }
      }

      return 0;
    });

    // Build array of plugin options for form table.
    $plugin_options = array_map(function ($plugin) use ($plugins) {
      // Display plugin name in one column.
      $option = [
        'name' => $plugin['name'],
        'dependencies' => '',
      ];

      // Build column for dependencies.
      $dependencies = $plugin['dependencies'] ?? [];
      if (!empty($dependencies)) {
        $dependency_names = array_map(function ($dependency) use ($plugins) {
          // If plugin is found, use human readable name.
          if (!empty($plugins[$dependency]['name'])) {
            return $plugins[$dependency]['name'];
          }
          // If the dependency wasn't found in the plugin list.
          else {
            return $dependency . ' (not found)';
          }
        }, $dependencies);

        // Build plugin dependency list markup..
        $option['dependencies'] = [
          'data' => [
            '#prefix' => '<ul class="dependency-list">',
            '#suffix' => '</ul>',
            '#markup' => '<li>' . implode('</li><li>', $dependency_names) . '</li>',
          ],
        ];
      }

      return $option;
    }, $plugins);

    return $plugin_options;
  }

  /**
   * {@inheritdoc}
   */
  public function manualSubmitForm(array &$form, FormStateInterface $form_state) {
    // Loop through selected migrations.
    $plugins = $form_state->getValue('migrations');
    // Filter out empty values.
    $plugins = array_filter($plugins);

    // Run migrations.
    $this->migrationActivation->manualMigration($plugins);

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function queueSubmitForm(array &$form, FormStateInterface $form_state) {
    // Loop through selected migrations.
    $plugins = $form_state->getValue('migrations');
    // Filter out empty values.
    $plugins = array_filter($plugins);

    // Queue migrations.
    $this->migrationActivation->queueMigration($plugins);

    parent::submitForm($form, $form_state);
  }

}
