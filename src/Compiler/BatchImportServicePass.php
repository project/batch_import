<?php

namespace Drupal\batch_import\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\Reference;
use Drupal\batch_import\BatchMigrationServices\MigrationServiceInterface;

/**
 * Compiler pass for automatically registering migration services.
 */
class BatchImportServicePass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    // Module directory name to search in.
    $service_dir = 'BatchMigrationServices';
    // List of all modules.
    $containerModules = $container->getParameter('container.modules');
    // Finder object for files.
    $finder = new Finder();

    // Loop through modules.
    foreach ($containerModules as $module) {
      // Check for migration service directory in module.
      $directory = dirname($module['pathname']) . '/src/' . $service_dir;
      if (is_dir($directory)) {
        // Get php files in directory and loop through them.
        $files = $finder->in($directory)->files()->name('*.php');
        foreach ($files as $file_info) {
          // Get class namespace.
          $namespace = $this->getFullNamespace($file_info->getPathname());
          $class = $namespace . '\\' . basename($file_info->getFilename(), '.php');

          // If already defined.
          if ($container->hasDefinition($class)) {
            continue;
          }
          // If class is abstract or class doesn't implement
          // MigrationServiceInterface.
          $reflect = new \ReflectionClass($class);
          if ($reflect->isAbstract() || !in_array(MigrationServiceInterface::class, class_implements($class))) {
            continue;
          }

          // Call class's id() function to get id string.
          $id = call_user_func([$class, 'id']);
          if (empty($id)) {
            continue;
          }
          // Full service id.
          $service_id = 'batch_import.migration.' . $id;

          // Call class's arguments() fuction to get list of
          // constructor argument ids.
          $arguments = call_user_func([$class, 'serviceArguments']);
          // Create new Definition for registering the service.
          $definition = new Definition($class);
          $definition->setClass($class);

          // Set service definition's arguments if they exist.
          if (!empty($arguments)) {
            $arguments = array_map(function ($arg) {
              return new Reference($arg);
            }, $arguments);

            $definition->setArguments($arguments);
          }

          // Register service.
          $container->setDefinition($service_id, $definition);
        }
      }
    }
  }

  /**
   * Get namespace from file contents.
   *
   * @param string $filename
   *   Filename string to retrieve namespace.
   *
   * @return string
   *   Namespace string for file.
   */
  private function getFullNamespace($filename) {
    // Get file contents and search for namespace keyword.
    $lines = file($filename);
    $lines_grep = preg_grep('/^namespace /', $lines);
    $namespace_line = array_shift($lines_grep);

    // Get full namespace for file.
    $match = [];
    preg_match('/^namespace (.*);$/', $namespace_line, $match);
    $full_namespace = array_pop($match);

    return $full_namespace;
  }

}
