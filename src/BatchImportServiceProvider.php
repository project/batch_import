<?php

namespace Drupal\batch_import;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\batch_import\Compiler\BatchImportServicePass;

/**
 * Add compiler pass to automatically register migration services.
 */
class BatchImportServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new BatchImportServicePass());
  }

}
