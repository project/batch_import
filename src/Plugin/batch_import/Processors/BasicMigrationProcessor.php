<?php

namespace Drupal\batch_import\Plugin\batch_import\Processors;

use Drupal\batch_import\Plugin\BatchMigrationProcessorBase;

/**
 * Plugin for basic migration processor.
 *
 * @BatchMigrationProcessor(
 *   id = "basic",
 * )
 */
class BasicMigrationProcessor extends BatchMigrationProcessorBase {}
