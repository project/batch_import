<?php

namespace Drupal\batch_import\Plugin\batch_import\Processors;

use Drupal\batch_import\Plugin\BatchMigrationProcessorBase;
use Drupal\batch_import\Plugin\BatchMigrationInterface;

/**
 * Plugin for entity migration processor.
 *
 * @BatchMigrationProcessor(
 *   id = "entity",
 * )
 */
class EntityMigrationProcessor extends BatchMigrationProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function processDestination(array $data, BatchMigrationInterface $migration) {
    // Create or load entity.
    $entity = $migration->getEntity($data);

    // Initialize entity.
    $migration->initEntity($entity, $data);

    // Run migration destination function for $entity.
    $entity = $migration->destination($data, ['entity' => $entity]);

    // Save $entity.
    $migration->saveEntity($entity, $data);

    return $entity;
  }

}
