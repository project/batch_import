<?php

namespace Drupal\batch_import\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines an interface for Batch Import migration plugins.
 */
interface BatchMigrationInterface extends PluginInspectionInterface {

  /**
   * Return the name of the migration.
   *
   * @return string
   *   Migration name.
   */
  public function name();

  /**
   * Return the processor of migration. Defaults to basic.
   *
   * @return string
   *   Migration processor.
   */
  public function processor();

  /**
   * Return the database id.
   *
   * @return string
   *   Database ID.
   */
  public function origin();

  /**
   * Return the entity type.
   *
   * @return string
   *   Entity Type.
   */
  public function entityType();

  /**
   * Return the entity id key.
   *
   * @return string
   *   Entity ID key.
   */
  public function entityIdKey();

  /**
   * Return the bundle id.
   *
   * @return string
   *   Bundle id.
   */
  public function bundle();

  /**
   * Return an array of dependencies.
   *
   * @return array
   *   List of migration plugin dependencies.
   */
  public function dependencies();

  /**
   * Return an array of migration service ids to inject.
   *
   * @return array
   *   List of migration service ids.
   */
  public function getServices();

  /**
   * Get migration service.
   *
   * @param string $id
   *   ID for migration service.
   *
   * @return \Drupal\batch_import\BatchMigrationServices\MigrationServiceInterface
   *   Migration service instance.
   */
  public function service($id);

  /**
   * Determine if migration service is injected.
   *
   * @param string $id
   *   ID for migration service.
   *
   * @return bool
   *   Return TRUE if migration service is injected, FALSE otherwise.
   */
  public function hasService($id);

  /**
   * Get the serialized data that represents an entity to be imported.
   *
   * @return array
   *   The origin data array.
   */
  public function source(): array;

  /**
   * Create or update an entity.
   *
   * @param array $data
   *   The info array.
   * @param array $args
   *   Args required for destination migration.
   *
   * @return mixed
   *   The results of destination migration.
   */
  public function destination(array $data, array $args = []);

  /**
   * Process retrieved data.
   *
   * @return array
   *   The data array.
   */
  public function processSource();

  /**
   * Process retrieved data.
   *
   * @param array $data
   *   The data array.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The imported entity.
   */
  public function processDestination(array $data);

  /**
   * Create or load an entity.
   *
   * @param array $data
   *   The info array.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The created or loaded entity.
   */
  public function getEntity(array &$data): ?EntityInterface;

  /**
   * Save entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   Entity recently created or loaded.
   * @param array $data
   *   The info array.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The savedentity.
   */
  public function saveEntity(ContentEntityBase $entity, array &$data);

  /**
   * Initialize entity for import.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   Entity recently created or loaded.
   * @param array $data
   *   The info array.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The initialized entity.
   */
  public function initEntity(ContentEntityBase $entity, array &$data);

}
