<?php

namespace Drupal\batch_import\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Batch Migration migration processor plugins.
 */
interface BatchMigrationProcessorInterface extends PluginInspectionInterface {

  /**
   * Process source functionality.
   *
   * @param \Drupal\batch_import\Plugin\BatchMigrationInterface $migration
   *   Migration that is currently running.
   */
  public function processSource(BatchMigrationInterface $migration);

  /**
   * Process data according to migration processor.
   *
   * @param array $data
   *   Single element of data.
   * @param \Drupal\batch_import\Plugin\BatchMigrationInterface $migration
   *   Migration that is currently running.
   */
  public function processDestination(array $data, BatchMigrationInterface $migration);

}
