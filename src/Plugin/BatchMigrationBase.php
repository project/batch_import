<?php

namespace Drupal\batch_import\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\batch_import\MigrationActivationService;
use Drupal\batch_import\BatchMigrationServices\EntityMigrationServiceInterface;
use Drupal\batch_import\BatchMigrationServices\LocalDatabaseTableService;
use Drupal\batch_import\BatchMigrationProcessorsPluginManager;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines a base class for Batch Import migration plugins.
 */
abstract class BatchMigrationBase extends PluginBase implements BatchMigrationInterface, ContainerFactoryPluginInterface {

  /**
   * Retrieves entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Migration activation service.
   *
   * @var \Drupal\batch_import\MigrationActivationService
   */
  protected $migrations;

  /**
   * List of automatically injected services.
   *
   * @var array
   */
  protected $services = [];

  /**
   * Migration processor plugin.
   *
   * @var \Drupal\batch_import\Plugin\BatchMigrationProcessorInterface
   */
  protected $processor;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Config array.
   * @param string $plugin_id
   *   Plugin id.
   * @param string $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity manager.
   * @param \Drupal\batch_import\MigrationActivationService $migration_activation
   *   Migration activation service.
   * @param \Drupal\batch_import\BatchMigrationProcessorsPluginManager $migration_processor_manager
   *   Migration processor manager service.
   * @param array $services
   *   Array of automatically injected services.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, MigrationActivationService $migration_activation, BatchMigrationProcessorsPluginManager $migration_processor_manager, array $services = []) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->services = $services;
    $this->entityManager = $entity_manager;
    $this->migrations = $migration_activation;

    // Get all migration plugin definitions.
    $plugins = $migration_processor_manager->getDefinitions();

    // Determine migration processor plugin id.
    $processor = array_key_exists($this->processor(), $plugins) ? $this->processor() : 'basic';

    // Create and set migration processor plugin instance.
    $this->processor = $migration_processor_manager->createInstance($processor);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('migration_activation'),
      $container->get('plugin.manager.batch_migration_processors'),
      self::serviceList($container, $plugin_definition),
    );
  }

  /**
   * Get list of migration service instances.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object for loading service.
   * @param string $plugin_definition
   *   Plugin definition.
   *
   * @return array
   *   Array of automatically injected migration services.
   */
  public static function serviceList(ContainerInterface $container, $plugin_definition) {
    // Default injected for all migration services.
    $service_keys = [
      'db_table',
    ];

    // Inject service associated with origin if it exists.
    if (!empty($plugin_definition['origin'])) {
      $service_keys[] = $plugin_definition['origin'];
    }
    // Inject service associated with entity type.
    if (!empty($plugin_definition['entity_type'])) {
      $service_keys[] = $plugin_definition['entity_type'];
    }

    // Inject services defined in annotations.
    $custom_services = $plugin_definition['services'] ?? [];

    // Merge service keys.
    $service_keys = array_merge($service_keys, $custom_services);
    $service_keys = array_unique($service_keys);

    // Load batch migration services.
    $services = [];
    foreach ($service_keys as $service_key) {
      // Full service key string.
      $full_key = 'batch_import.migration.' . $service_key;
      // Inject service instance.
      $services[$service_key] = $container->get($full_key);
    }

    return $services;
  }

  /**
   * {@inheritdoc}
   */
  public function name() {
    return $this->pluginDefinition['name'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function processor() {
    return $this->pluginDefinition['processor'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function origin() {
    return $this->pluginDefinition['origin'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function entityType() {
    return $this->pluginDefinition['entity_type'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function entityIdKey() {
    // If entity id key is defined in annotations, use specified id key.
    if (!empty($this->pluginDefinition['entity_id_key'])) {
      return $this->pluginDefinition['entity_id_key'];
    }
    // Otherwise load entity id key automatically.
    else {
      // Get storage from entity type.
      $entity_type = $this->entityType();
      $entityStorage = $this->entityManager->getStorage($entity_type);
      // Get entity type definition.
      $type = $entityStorage->getEntityType();
      // Entity key properties.
      $entity_keys = $type->getKeys();

      // Get id key from entity keys.
      return $entity_keys['id'] ?? NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function bundle() {
    return $this->pluginDefinition['bundle'] ?? $this->entityType();
  }

  /**
   * {@inheritdoc}
   */
  public function dependencies() {
    return $this->pluginDefinition['dependencies'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getServices() {
    return $this->services ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function service($key) {
    return $this->services[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasService($key) {
    return !empty($this->services[$key]);
  }

  /**
   * Get the arcade migration database connection.
   *
   * @param string $database_id
   *   Origin id. If unspecified, get value from annotations.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  protected function getConnection($database_id = NULL): Connection {
    // If $database_id is empty, use value from annotations.
    if (empty($database_id)) {
      $database_id = $this->origin();
    }
    // Set active connection and return.
    Database::setActiveConnection($database_id);
    return Database::getConnection();
  }

  /**
   * {@inheritdoc}
   */
  public function processSource() {
    return $this->processor->processSource($this);
  }

  /**
   * {@inheritdoc}
   */
  public function processDestination(array $data) {
    return $this->processor->processDestination($data, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(array &$data): ?EntityInterface {
    // Get Annotation values.
    $entity_type = $this->entityType();

    // Load entity migration service if it exists.
    $entity_migration_service = $this->service($entity_type);
    if (!empty($entity_migration_service) && $entity_migration_service instanceof EntityMigrationServiceInterface) {
      // Load or create entity using entity migration service.
      $entity = $entity_migration_service->getEntity($data, $this->bundle(), $this->entityIdKey(), $this->origin());
    }

    return $entity ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function saveEntity($entity, array &$data) {
    // Get Annotation values.
    $entity_type = $this->entityType();

    // Load entity migration service if it exists.
    $entity_migration_service = $this->service($entity_type);
    if (!empty($entity_migration_service) && $entity_migration_service instanceof EntityMigrationServiceInterface) {
      // Save entity using entity migration service.
      $entity_migration_service->save($entity, $data);

      // Get entity id key.
      $source_id = $this->entityIdKey();

      // Save entity to batch_import local db table.
      $db_table = $this->service('db_table');
      if (!empty($db_table) && $db_table instanceof LocalDatabaseTableService) {
        $db_table->save($data[$source_id], $entity->id(), $entity_type, $this->bundle(), $this->origin());
      }
    }

    return $entity ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function initEntity(ContentEntityBase $entity, array &$data) {
    // Get Annotation value.
    $entity_type = $this->entityType();

    // Load entity migration service if it exists.
    $entity_migration_service = $this->service($entity_type);
    if (!empty($entity_migration_service) && $entity_migration_service instanceof EntityMigrationServiceInterface) {
      // Initialize entity.
      $entity_migration_service->initEntity($entity, $data, $this->origin());
    }
    return $entity;
  }

}
