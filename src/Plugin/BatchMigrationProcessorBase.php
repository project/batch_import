<?php

namespace Drupal\batch_import\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Defines a base class for Batch Import migration processor plugins.
 */
abstract class BatchMigrationProcessorBase extends PluginBase implements BatchMigrationProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processSource(BatchMigrationInterface $migration) {
    // Run migration source function.
    return $migration->source();
  }

  /**
   * {@inheritdoc}
   */
  public function processDestination(array $data, BatchMigrationInterface $migration) {
    // Run migration destination function for $data.
    return $migration->destination($data);
  }

}
