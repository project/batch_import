<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Database;
use Drupal\batch_import\MigrationActivationService;

/**
 * Provides Base parsing and saving functionality.
 */
abstract class BatchImportDestinationBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Migration activation service.
   *
   * @var \Drupal\batch_import\MigrationActivationService
   */
  protected $migrationActivation;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Config array.
   * @param string $plugin_id
   *   Plugin id.
   * @param string $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity type manager object.
   * @param \Drupal\batch_import\MigrationActivationService $migration_activation
   *   Migration activation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, MigrationActivationService $migration_activation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->migrationActivation = $migration_activation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('migration_activation')
    );
  }

  /**
   * Processes an item in the queue.
   *
   * @param object $source_data
   *   Single entity data from source.
   */
  public function processItem($source_data) {
    if (isset($source_data->plugin_id)) {
      $plugin_id = $source_data->plugin_id;
      $data = $source_data->data;

      // Process source data entity.
      $this->migrationActivation->processData($plugin_id, $data);
    }
  }

  /**
   * Helper function to transform the human readable string into a machine name.
   *
   * @param string $str
   *   Human readable string.
   *
   * @return string
   *   Machine readable string.
   */
  protected function toMachineName($str) {
    $machine_str = strtolower($str);
    $machine_str = preg_replace('/[^a-z0-9_]+/', '_', $machine_str);
    $machine_str = preg_replace('/_+/', '_', $machine_str);
    return $machine_str;
  }

  /**
   * Helper function to check if the entity has previously migrated.
   *
   * @param int $entity_id
   *   Source entity id.
   * @param string $bundle
   *   Bundle to look up.
   *
   * @return int|false
   *   destination entity id if exists.
   */
  protected function checkMigrationMap($entity_id, $bundle) {
    // Check local db table.
    $db = Database::getConnection('default', 'default');
    $query = $db->select('batch_import', 'm');
    $query->condition('m.eid', $entity_id);
    $query->condition('m.bundle', $bundle);
    // $query->condition('m.lang', $lang);
    $query->fields('m', ['eid']);

    // Execute database query.
    $result = $query->execute()->fetchAll();
    if (!empty($result)) {
      // Return first eid.
      $result = reset($res);
      return $result->eid;
    }
    else {
      return FALSE;
    }
  }

}
