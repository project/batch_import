<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\batch_import\MigrationActivationService;

/**
 * Base class for batch import Source class.
 */
abstract class BatchImportSourceBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Query factory object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Queue manager object.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * Migration activation service.
   *
   * @var \Drupal\batch_import\MigrationActivationService
   */
  protected $migrationActivation;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Config array.
   * @param string $plugin_id
   *   Plugin id.
   * @param string $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   Queue Factory object.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   Queue manager object.
   * @param \Drupal\batch_import\MigrationActivationService $migration_activation
   *   Migration activation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueueFactory $queue, QueueWorkerManagerInterface $queue_manager, MigrationActivationService $migration_activation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queue;
    $this->queueManager = $queue_manager;
    $this->migrationActivation = $migration_activation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('migration_activation')
    );
  }

  /**
   * Return queue object for destination processing.
   *
   * @return \Drupal\Core\Queue\QueueInterface
   *   Queue for destination processing.
   */
  abstract public function getDestinationQueue();

  /**
   * Process item in the queue.
   *
   * @param object $data
   *   Array of all data to be imported.
   */
  public function processItem($data) {
    $queue_destination = $this->getDestinationQueue();

    // Get migration plugin id.
    $plugin_id = $data->plugin_id;
    $plugin_definition = $data->plugin_definition;

    // Get migration source data.
    $data = $this->migrationActivation->getData($plugin_id);

    if (!empty($data)) {
      // Loop through all data.
      $index = 0;
      foreach ($data as $row) {
        $values = [];

        // Format field keys in row.
        foreach ($row as $key => $field) {
          $new_key = str_replace(' ', '_', $key);
          $values[$new_key] = $field;
        }

        if (!empty($values)) {
          $item = new \stdClass();
          $item->data = $values;
          $item->plugin_id = $plugin_id;
          $item->plugin_definition = $plugin_definition;
          $item->index = $index;
          $item->total = count($data);

          // Add to queue.
          $queue_destination->createItem($item);
        }
        $index++;
      }
    }
  }

}
