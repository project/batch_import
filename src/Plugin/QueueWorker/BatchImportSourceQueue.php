<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

/**
 * Provides Base parsing functionality for the CSV files.
 *
 * @QueueWorker(
 *   id = "batch_import_source_queue",
 *   title = @Translation("Retrieving source data"),
 *   cron = {"time" = 10}
 * )
 */
class BatchImportSourceQueue extends BatchImportSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getDestinationQueue() {
    return $this->queueFactory->get('batch_import_destination_queue', TRUE);
  }

}
