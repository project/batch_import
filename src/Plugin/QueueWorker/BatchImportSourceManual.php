<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

/**
 * Provides Base parsing functionality for the CSV files.
 *
 * @QueueWorker(
 *   id = "batch_import_source_manual",
 *   title = @Translation("Retrieving source data")
 * )
 */
class BatchImportSourceManual extends BatchImportSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getDestinationQueue() {
    return $this->queueFactory->get('batch_import_destination_manual', TRUE);
  }

}
