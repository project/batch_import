<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

/**
 * Provides destination functionality for content.
 *
 * @QueueWorker(
 *   id = "batch_import_destination_queue",
 *   title = @Translation("Processing destination data"),
 *   cron = {"time" = 10}
 * )
 */
class BatchImportDestinationQueue extends BatchImportDestinationBase {}
