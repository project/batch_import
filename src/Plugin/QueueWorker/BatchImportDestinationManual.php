<?php

namespace Drupal\batch_import\Plugin\QueueWorker;

/**
 * Provides destination functionality for content.
 *
 * @QueueWorker(
 *   id = "batch_import_destination_manual",
 *   title = @Translation("Processing destination data")
 * )
 */
class BatchImportDestinationManual extends BatchImportDestinationBase {}
