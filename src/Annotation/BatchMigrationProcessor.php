<?php

namespace Drupal\batch_import\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a migration processor annotation object.
 *
 * Plugin Namespace: Plugin\batch_import\MigrationProcessors.
 *
 * @see \Drupal\batch_import\BatchMigrationProcessorsPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class BatchMigrationProcessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
