<?php

namespace Drupal\batch_import\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a migration item annotation object.
 *
 * Plugin Namespace: Plugin\batch_import\Migrations.
 *
 * @see \Drupal\batch_import\BatchMigrationsPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class BatchMigration extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the migration.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * What processor to use for migration, defaults to basic.
   *
   * @var string
   */
  public $processor;

  /**
   * The source database used for the migration.
   *
   * @var string
   */
  public $origin;

  /**
   * Entity type being imported. Ex. node, taxonomy_term, etc.
   *
   * @var string
   */
  public $entity_type;

  /**
   * String used as entity id. Ex. nid, tid, etc.
   *
   * @var string
   */
  public $entity_id_key;

  /**
   * Bundle for entity type being imported.
   *
   * @var string
   */
  public $bundle;

  /**
   * List of migration services to automatically inject.
   *
   * @var array
   */
  public $services;

  /**
   * Migrations this one is dependent on.
   *
   * @var array
   */
  public $dependencies;

  /**
   * Whether or not to show migration on migration form.
   *
   * @var bool
   */
  public $hidden;

}
