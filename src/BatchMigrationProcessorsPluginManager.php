<?php

namespace Drupal\batch_import;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Batch Migration Processors plugin manager.
 */
class BatchMigrationProcessorsPluginManager extends DefaultPluginManager {

  /**
   * Constructs an BatchMigrationProcessorsPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/batch_import/Processors',
      $namespaces,
      $module_handler,
      'Drupal\batch_import\Plugin\BatchMigrationProcessorInterface',
      'Drupal\batch_import\Annotation\BatchMigrationProcessor'
    );

    $this->alterInfo('batch_import_migration_processors_info');
    $this->setCacheBackend($cache_backend, 'batch_import_migration_processors');
  }

}
