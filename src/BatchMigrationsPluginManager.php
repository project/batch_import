<?php

namespace Drupal\batch_import;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Batch Import plugin manager.
 */
class BatchMigrationsPluginManager extends DefaultPluginManager {

  /**
   * Constructs an BatchMigrationsPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/batch_import/Migrations',
      $namespaces,
      $module_handler,
      'Drupal\batch_import\Plugin\BatchMigrationInterface',
      'Drupal\batch_import\Annotation\BatchMigration'
    );

    $this->alterInfo('batch_import_migrations_info');
    $this->setCacheBackend($cache_backend, 'batch_import_migrations');
  }

}
