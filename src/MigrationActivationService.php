<?php

namespace Drupal\batch_import;

use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Utility\Error;

/**
 * Migration activation service.
 */
class MigrationActivationService {
  use StringTranslationTrait;
  // @todo Look into removing this.
  use DependencySerializationTrait;

  /**
   * Migration plugin manager.
   *
   * @var \Drupal\batch_import\BatchMigrationsPluginManager
   */
  protected $pluginManager;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManager
   */
  protected $queueManager;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor.
   *
   * @param \Drupal\batch_import\BatchMigrationsPluginManager $plugin_manager
   *   Migration plugin manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue Factory object.
   * @param \Drupal\Core\Queue\QueueWorkerManager $queue_manager
   *   Queue manager object.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(BatchMigrationsPluginManager $plugin_manager, QueueFactory $queue_factory, QueueWorkerManager $queue_manager, MessengerInterface $messenger) {
    $this->pluginManager = $plugin_manager;
    $this->queueFactory = $queue_factory;
    $this->queueManager = $queue_manager;
    $this->messenger = $messenger;
  }

  /**
   * Get instance of specified migration plugin.
   *
   * @param string $plugin_id
   *   ID for migration plugin.
   *
   * @return \Drupal\batch_import\Plugin\BatchMigrationInterface
   *   Migration plugin instance.
   */
  protected function getPlugin($plugin_id) {
    // Get all migration plugin definitions.
    $plugins = $this->pluginManager->getDefinitions();

    if (array_key_exists($plugin_id, $plugins)) {
      // Create  and return instance.
      return $this->pluginManager->createInstance($plugin_id);
    }

    return NULL;
  }

  /**
   * Retrieve data using specified migration plugin.
   *
   * @param string $plugin_id
   *   ID for migration plugin.
   *
   * @return array
   *   Data retrieved by migration plugin.
   */
  public function getData($plugin_id) {
    $data = [];

    // Get migration plugin instance.
    if ($plugin = $this->getPlugin($plugin_id)) {
      // Get source data.
      $data = $plugin->processSource();
    }

    return $data;
  }

  /**
   * Process data using specified migration plugin.
   *
   * @param string $plugin_id
   *   ID for migration plugin.
   * @param array $data
   *   Data imported from source.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return entity instance if it exists.
   */
  public function processData($plugin_id, array $data) {
    if ($plugin = $this->getPlugin($plugin_id)) {
      return $plugin->processDestination($data);
    }

    return NULL;
  }

  /**
   * Function to populate Queue with data from D* database.
   *
   * @param string|array $plugin_ids
   *   ID for migration plugin or array of ids.
   */
  public function queueMigration($plugin_ids) {
    // Get source queue instance.
    $queue_source = $this->queueFactory->get('batch_import_source_queue', TRUE);

    // If $plugin_ids is a single id string, put it into an array.
    if (is_string($plugin_ids)) {
      $plugin_ids = [$plugin_ids];
    }

    // All migration plugin definitions.
    $plugins = $this->pluginManager->getDefinitions();

    $index = 0;
    foreach ($plugin_ids as $plugin_id) {
      // Load plugin definition.
      $plugin_definition = array_key_exists($plugin_id, $plugins) ? $plugins[$plugin_id] : NULL;

      // Queue item object.
      $item = new \stdClass();
      $item->plugin_id = $plugin_id;
      $item->plugin_definition = $plugin_definition;
      $item->index = $index;
      $item->total = count($plugin_ids);

      // Queue migration plugin.
      $queue_source->createItem($item);
      $index++;
    }
  }

  /**
   * Function to populate Queue with data from D* database.
   *
   * @param string|array $plugin_ids
   *   ID for migration plugin or array of ids.
   */
  public function manualMigration($plugin_ids) {
    // Clear source queue instance.
    $queue_source = $this->queueFactory->get('batch_import_source_manual', TRUE);
    $queue_source->deleteQueue();
    // Clear destination queue instance.
    $queue_destination = $this->queueFactory->get('batch_import_destination_manual', TRUE);
    $queue_destination->deleteQueue();

    // If $plugin_ids is a single id string, put it into an array.
    if (is_string($plugin_ids)) {
      $plugin_ids = [$plugin_ids];
    }

    // All migration plugin definitions.
    $plugins = $this->pluginManager->getDefinitions();

    // Array of queue operations.
    $operations = [];

    $index = 0;
    foreach ($plugin_ids as $plugin_id) {
      // Load plugin definition.
      $plugin_definition = array_key_exists($plugin_id, $plugins) ? $plugins[$plugin_id] : NULL;

      // Queue item object.
      $item = new \stdClass();
      $item->plugin_id = $plugin_id;
      $item->plugin_definition = $plugin_definition;
      $item->index = $index;
      $item->total = count($plugin_ids);

      // Queue migration plugin.
      $queue_source->createItem($item);
      $index++;
    }

    // Getting data from source.
    $operations[] = [
      [$this, 'queueProcessItem'],
      [
        'batch_import_source_manual',
      ],
    ];
    // Processing data on destination.
    $operations[] = [
      [$this, 'queueProcessItem'],
      [
        'batch_import_destination_manual',
      ],
    ];

    // Add batch operations.
    $batch = [
      'title' => $this->t('Batch Import'),
      'operations' => $operations,
      'finished' => [$this, 'outputResults'],
      'progress_message' => $this->t('Completed @current of @total operations.'),
    ];
    batch_set($batch);
  }

  /**
   * Helper function to manually process queued items.
   *
   * @param string $queue_name
   *   Name of the queue.
   * @param array $context
   *   Context array.
   */
  public function queueProcessItem($queue_name, array &$context) {
    // Get queue.
    $queue = $this->queueFactory->get($queue_name, TRUE);
    // Get queue worker.
    $queue_worker = $this->queueManager->createInstance($queue_name);
    // Batch limit.
    $limit = 1;

    // If this is the first in the queue.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current'] = 0;
      $context['sandbox']['max'] = $queue->numberOfItems();
      $context['sandbox']['offset'] = 0;
      $context['results'][$queue_name]['success']['count'] = 0;
    }
    else {
      $context['sandbox']['offset'] += $limit;
    }

    // Get plugin definition for queue worker.
    $queue_worker_definition = $queue_worker->getPluginDefinition();

    // Loop through items in this queue, limited by $limit for batching.
    $count = min($context['sandbox']['offset'] + $limit, $context['sandbox']['max']);
    for ($i = $context['sandbox']['offset']; $i < $count; $i++) {
      // Get queue item.
      $item = $queue->claimItem();

      if (isset($item) && isset($item->data)) {
        try {
          // Get migration plugin information.
          $plugin_id = $item->data->plugin_id ?? NULL;
          $plugin_definition = $item->data->plugin_definition ?? NULL;

          // Process and remove queue item.
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);

          // Add to the count.
          $context['results'][$queue_name]['success']['count']++;

          // If migration plugin info is available.
          if (!empty($plugin_id)) {
            if (empty($context['results'][$queue_name]['migrations'][$plugin_id])) {
              // Batch information for finished output.
              $context['results'][$queue_name]['migrations'][$plugin_id] = [
                'plugin_id' => $plugin_id,
                'label' => $plugin_definition['name'] ?? '',
                'count' => 0,
              ];
            }

            // Track batch count for this specific migration plugin.
            $context['results'][$queue_name]['migrations'][$plugin_id]['count']++;
          }

          // Set current batch information.
          $context['message'] = $this->t('Current operation: @migration - @queue (@progress of @total)', [
            '@migration' => $plugin_definition['name'] ?? $this->t('Migration'),
            '@queue' => $queue_worker_definition['title'],
            '@progress' => $item->data->index ?? $i,
            '@total' => $item->data->total ?? $context['sandbox']['max'],
          ]);
        }
        catch (SuspendQueueException $e) {
          // If there was an error, remove from queue.
          $queue->releaseItem($item);
          $context['results'][$queue_name]['errors'][] = $e;
        }
        catch (\Exception $e) {
          $logger = \Drupal::logger('batch_import');
          Error::logException($logger, $e);
          echo $e;
          $context['results'][$queue_name]['errors'][] = $e;
        }
      }
      $context['sandbox']['progress']++;
    }

    // Progress percentage.
    if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    // If progress is finished.
    else {
      $context['sandbox'] = [];
      $context['finished'] = 1;
    }
  }

  /**
   * Finished callback for batch.
   *
   * @param bool $success
   *   True if successful, false otherwise.
   * @param array $results
   *   Array of results data.
   * @param array $operations
   *   Array of batch operations.
   */
  public function outputResults($success, array $results, array $operations) {
    // If success count is set.
    if (isset($results['batch_import_destination_manual']['success']['count']) && !empty($results['batch_import_destination_manual']['migrations'])) {
      foreach ($results['batch_import_destination_manual']['migrations'] as $migration) {
        // Finished message for migration plugin.
        $message = $this->t('@migration: Successfully processed @count entities', [
          '@migration' => $migration['label'],
          '@count' => $migration['count'],
        ]);
        $this->messenger->addMessage($message, 'status');
      }
    }
    // If there were errors.
    if (isset($results['batch_import_destination_manual']['errors'])) {
      foreach ($results['batch_import_destination_manual']['errors'] as $error) {
        $this->messenger->addMessage($error, 'error');
      }
    }
  }

  /**
   * Run migration plugins without queueing or batching.
   *
   * @param array $plugin_ids
   *   Array of plugin ids to process.
   */
  public function runMigrationPlugins(array $plugin_ids) {
    foreach ($plugin_ids as $plugin_id) {
      $data = $this->getData($plugin_id);

      // Loop through data rows.
      foreach ($data as $d) {
        $this->processData($plugin_id, (array) $d);
      }
    }
  }

}
