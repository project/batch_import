# Batch Import

This module allows Drupal developers to create migration class files that run
in a batch/queue system.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/batch_import).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/batch_import).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules]
(https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Set up source database query method. For example, in settings.php define
the source database array:
```
 $databases['source']['default'] = [
  'database' => 'source_db_name',
  'username' => 'source_db_user',
  'password' => 'source_db_pwd',
  'host' => 'source_db_host,
  'driver' => 'source_db_driver',
  'port' => 'source_db_port,
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];
```
In addition, any files to import as media should already be in
/sites/default/files, or brought in as a migration.


Migration classes are the core of custom migration functionality. Each class
adds a new migration option to the Batch Import Administration page. In the
migration class, the **source** and **destination** functions
should be overloaded.

The processor will prepare data between source and destination. For example,
the **entity** processor will automatically load or create entities loaded
from **source** before they are passed into the **destination** function.

Some migration services are automatically injected, such as db_table,
the migration service related to the entity type, and the migration service
related to the origin if it exists. Any other services to inject should be in
the **services** property in annotations.
To create a migration:
1. Create or edit a custom module
1. In the directory **/my_module/src/Plugin/batch_import/Migrations/** create a
new migration class file, for example:
**/my_module/src/Plugin/batch_import/Migrations/PageMigration.php**
1. The new class should extend
**Drupal\batch_import\Plugin\BatchMigrationBase**
1. The plugin properties are defined in annotions. The comment above
the class definition should look something like this:
```
/**
 * MyBasicMigration class.
 *
 * @BatchMigration(
 *   id = "my_basic_migration",
 *   name = @Translation("My Basic Migration"),
 * )
 */
```
The following annotation properties are required:
* id - machine name for the migration
* name - Human readable name for the migration

The following annotation properties are optional:
* origin - source database id, should be the key value in the $databases
array if set.
* processor - custom processor entity that handles data between source and
destination.
* entity_type - used by entity processor for entity type, such as node,
taxonomy_term, user, etc.
* entity_id_key - id key such as nid for node, tid for taxonomy_term, etc.
System will try to automatically detect depending on entity_type, but may need
to be overloaded.
* bundle - used by entity processor for bundle type.
 Here is an example of a migration for basic page nodes:
* dependencies - array of machine names for other migrations that should run
before this one.
* services - array of migration services to automatically inject.
* hidden - set to true if migration should be hidden on migration page.
```
 /**
 * PageMigration class.
 *
 * @BatchMigration(
 *   id = "page_migration",
 *   processor = "entity",
 *   name = @Translation("Page Migration"),
 *   origin = "source",
 *   entity_type = "node",
 *   bundle = "page",
 *   dependencies = {
 *     "user_migration",
 *     "media_image_migration",
 *   },
 *   services = {
 *     "taxonomy_term",
 *     "media"
 *   }
 * )
 */
```
5. Overload the source and destination functions



Migration services are meant to include any shared functionality shared across
migrations. To create a migration service:
1. Create or edit a custom module (preferably same module as migration classes)
1. In the directory **/my_module/src/BatchMigrationServices/** create a new
migration class file, for example:
**/my_module/src/BatchMigrationServices/CustomMigrationService.php**
1. The class should extend
**Drupal\batch_import\BatchMigrationServices\MigrationServiceBase** and
overload the id() function for the machine name id.


Processors should handle data being passed into and out of **source()** and
**destination()**. To create a processor:
1. Create or edit a custom module (preferably same module as migration classes)
1. In the directory **/my_module/src/Plugin/batch_import/Processors/** create a
new migration class file, for example:
**/my_module/src/Plugin/batch_import/Processors/PageMigration.php**
1. The class should extend
**Drupal\batch_import\Plugin\BatchMigrationProcessorBase**. The plugin has the
following annotation:
    - id - machine name id string.
1. Overload **processSource()** and/or **processDestination()**. Be sure to
call **source()** and **destination()** inside, respectfully.


After migrations are created, go to **/admin/content/batch-import** and use the
form to execute migrations as desired.


## Maintainers

- Kelly - [kelly.m.jacobs](https://www.drupal.org/u/kellymjacobs)
